#! /bin/bash

deps=(
	/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
	alacritty
	blueman-applet
	brightnessctl
	bmenu-run
	code
	firefox
	gammastep
	grim
	nemo
	nm-applet
	nnn
	pactl
	pamixer
	playerctl
	swaymsg
	swaynag
	swaync
	swaync-client
	wl-copy
	wob
)

for i in "${deps[@]}"; do
	found=$(
		type "$i" &>/dev/null &&
			echo 1 || echo 0
	)
	if [ $found -eq 0 ]; then
		echo "$i not found"
	fi
done

#for i in "${deps[@]}"; do
#	if type "$i" &>/dev/null; then
#		found=1
#	else
#		found=0
#	fi
#	echo "$i found: $found"
#done
